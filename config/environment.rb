# Load the Rails application.
require File.expand_path('../application', __FILE__)

Rails.application.configure do
  # Setup multitenancy
  case ENV["CUSTOMER_NAME"]
  when 'CHECKMARX'
    config.authentication = :checkmarx_sso
  else
    config.authentication = :sso
  end
end

# Initialize the Rails application.
Rails.application.initialize!

