Rails.application.routes.draw do

  devise_for :users

  if Rails.configuration.authentication == :checkmarx_sso
    get '/saml/auth' => 'saml_idp_checkmarx#new'
    post '/saml/auth' => 'saml_idp_checkmarx#create'
  else
    get '/saml/auth' => 'saml_idp#new'
    post '/saml/auth' => 'saml_idp#create'
  end
end
