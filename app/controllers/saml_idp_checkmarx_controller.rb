class SamlIdpCheckmarxController < SamlIdp::IdpController

  def idp_authenticate(email, password)
    true
  end

  def idp_make_saml_response(user)
    # Simulation of a trial tier customer
    # provider = %[<saml:AttributeStatement><saml:Attribute Name="sales_force_id"><saml:AttributeValue>SALES_FORCE_ID_TRIAL_1</saml:AttributeValue></saml:Attribute><saml:Attribute Name="permissions"><saml:AttributeValue>StatisticsView</saml:AttributeValue></saml:Attribute><saml:Attribute Name="is_trial"><saml:AttributeValue>true</saml:AttributeValue></saml:Attribute><saml:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><saml:AttributeValue>customer_1@example.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement>]
    # provider = %[<saml:AttributeStatement><saml:Attribute Name="sales_force_id"><saml:AttributeValue>SALES_FORCE_ID_10001</saml:AttributeValue></saml:Attribute><saml:Attribute Name="permissions"><saml:AttributeValue>StatisticsView</saml:AttributeValue></saml:Attribute><saml:Attribute Name="is_trial"><saml:AttributeValue>true</saml:AttributeValue></saml:Attribute><saml:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><saml:AttributeValue>admin_1@demo.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement>]
    # provider = %[<saml:AttributeStatement><saml:Attribute Name="sales_force_id"><saml:AttributeValue>SALES_FORCE_ID_10002</saml:AttributeValue></saml:Attribute><saml:Attribute Name="permissions"><saml:AttributeValue>StatisticsView</saml:AttributeValue></saml:Attribute><saml:Attribute Name="is_trial"><saml:AttributeValue>false</saml:AttributeValue></saml:Attribute><saml:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><saml:AttributeValue>admin@cx4.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement>]
    # provider = %[<saml:AttributeStatement><saml:Attribute Name="sales_force_id"><saml:AttributeValue>SALES_FORCE_ID_10002</saml:AttributeValue></saml:Attribute><saml:Attribute Name="permissions"></saml:Attribute><saml:Attribute Name="is_trial"><saml:AttributeValue>true</saml:AttributeValue></saml:Attribute><saml:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><saml:AttributeValue>user_1_SF_10002@cx.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement>]
    # provider = %[<saml:AttributeStatement><saml:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><saml:AttributeValue>jp_user_2@jp.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement>]
    # provider = %[<saml:AttributeStatement><saml:Attribute Name="sales_force_id"><saml:AttributeValue>SALES_FORCE_ID_10004</saml:AttributeValue></saml:Attribute><saml:Attribute Name="permissions"></saml:Attribute><saml:Attribute Name="is_trial"><saml:AttributeValue>true</saml:AttributeValue></saml:Attribute><saml:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><saml:AttributeValue>superadmin_1@cx.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement>]
    provider = %[<saml:AttributeStatement><saml:Attribute Name="sales_force_id"><saml:AttributeValue>SALES_FORCE_ID_10005</saml:AttributeValue></saml:Attribute><saml:Attribute Name="permissions"><saml:AttributeValue>StatisticsView</saml:AttributeValue></saml:Attribute><saml:Attribute Name="is_trial"><saml:AttributeValue>false</saml:AttributeValue></saml:Attribute><saml:Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"><saml:AttributeValue>admin@cx4.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement>]
    encode_SAMLResponse('user_1_SF_10002@cx.com', { attributes_provider: provider })
  end
end

